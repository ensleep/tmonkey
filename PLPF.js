// ==UserScript==
// @name         批量打分
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  私用工具
// @updateURL  	https://gitee.com/ensleep/tmonkey/raw/master/PLPF.js.js
// @downloadURL	https://gitee.com/ensleep/tmonkey/raw/master/PLPF.js.js
// @author       ensleep
// @include        http://172.20.9.212:9501/Web/index.html*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    var c=setInterval(function(){
        let testel=document.querySelector("#app > div > div.main-container.hasTagsView > section > div > div > div > div > div > div > div.el-table__fixed-right > div.el-table__fixed-body-wrapper > table > tbody > tr:nth-child(1) > td.el-table_1_column_12.is-hidden > div");
        if(testel==null){
            return;
        }
        let btn=document.createElement('button');
        btn.innerText="自动打分";
        btn.type="button";
        btn.onclick=function(){
            let rows = document.querySelectorAll("#app > div > div.main-container.hasTagsView > section > div > div > div > div > div > div > div.el-table__fixed-right > div.el-table__fixed-body-wrapper > table > tbody > tr");
            for(let i =1;i<=rows.length;i++){

                let row=document.querySelector("#app > div > div.main-container.hasTagsView > section > div > div > div > div > div > div > div.el-table__fixed-right > div.el-table__fixed-body-wrapper > table > tbody > tr:nth-child("+i+") > td.el-table_1_column_12.is-hidden > div");
                let vrow=document.querySelector("#app > div > div.main-container.hasTagsView > section > div > div > div > div > div > div > div.el-table__fixed-right > div.el-table__fixed-body-wrapper > table > tbody > tr:nth-child("+i+") > td.el-table_1_column_13 > div > input");
				vrow.value=row.innerHTML;
                let event = new InputEvent('input');
                vrow.dispatchEvent(event);
            }
            return false;
        }
        btn.style.position="absolute";
        btn.style.top="3em";
        btn.style.right="0px";
        btn.style.zIndex="99999";
        //btn.style.background="greenyellow";
        //1btn.style.border="none";
        btn.style.height="3em";
        document.body.appendChild(btn);
        clearInterval(c);
    },500);
})();