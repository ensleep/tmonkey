// ==UserScript==
// @name         百年题目练习
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  私用工具
// @author       ensleep
// @updateURL  	https://gitee.com/ensleep/tmonkey/raw/master/100RedYearVote.js
// @downloadURL	https://gitee.com/ensleep/tmonkey/raw/master/100RedYearVote.js
// @match      http://app2.jschina.com.cn/2021dsdt/vote.php*
// @match      http://app2.jschina.com.cn/2021dsdt/vote1.php*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    let btn
    // Your code here...
    var c=setInterval(function(){
        let v="未识别"
        if(btn!=null){
            btn.remove()
        }

        var tm = document.querySelector(".swiper-slide-visible .contbox .ab").classList.value.split(" ").filter(x=>x.indexOf("item_")>-1)
        if(tm==null || tm.lenght==0){
            return
        }

        let key=tm[0].replace("item_","")

        v=document.querySelector("div.swiper-slide-visible input[id='key_"+key+"']").value;

        btn=document.createElement('button');
        btn.innerText='答案为：'+v;
        btn.type="div";
        btn.style.position="fixed";
        btn.style.top="3em";
        btn.style.right="0px";
        btn.style.zIndex="99999";
        //btn.style.background="greenyellow";
        //1btn.style.border="none";
        btn.style.height="3em";
        document.body.appendChild(btn);
    },500);
})();